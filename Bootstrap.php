<?php

namespace singletonn\blog;

use Yii;
use yii\base\BootstrapInterface;
use yii\web\Application;

class Bootstrap implements BootstrapInterface
{

    public static $bootstrap = true;

    /** @inheritdoc */
    public function bootstrap($app)
    {
        /** @var Module $module */
        if($app instanceof Application) {
            $module = $app->getModule('blog');
            $configUrlRule = [
                'prefix' => $module->urlPrefix,
                'rules' => $module->urlRules,
            ];

            $configUrlRule['class'] = 'yii\web\GroupUrlRule';
            $rule = Yii::createObject($configUrlRule);

            $app->urlManager->addRules([$rule], false);
        }
    }
}
