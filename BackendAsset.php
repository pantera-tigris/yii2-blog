<?php

namespace singletonn\blog;

use yii\web\AssetBundle;

/**
 * Main backend application asset bundle.
 */
class BackendAsset extends AssetBundle
{
    public $sourcePath = '@blog/assets';
    public $css = [
        'css/backend.css'
    ];
    public $js = [

    ];
    public $depends = [
        'yii\web\YiiAsset',
    ];
}
