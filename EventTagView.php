<?php
/**
 * Created by PhpStorm.
 * User: singletonn
 * Date: 9/10/18
 * Time: 1:52 PM
 */

namespace singletonn\blog;


use singletonn\blog\models\BlogTags;
use yii\base\Event;

class EventTagView extends Event
{
    /* @var BlogTags */
    public $model;
}