<?php

namespace singletonn\blog\controllers;

use singletonn\blog\FrontendAsset;
use Yii;

class Controller extends \yii\web\Controller
{

    public function beforeAction($action)
    {
        FrontendAsset::register(Yii::$app->view);
        return parent::beforeAction($action);
    }

}