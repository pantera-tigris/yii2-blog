<?php

namespace singletonn\blog\controllers\admin;

use singletonn\blog\BackendAsset;
use Yii;

class Controller extends \yii\web\Controller
{

    public function beforeAction($action)
    {
        BackendAsset::register(Yii::$app->view);
        return parent::beforeAction($action);
    }

}