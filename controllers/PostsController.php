<?php

namespace singletonn\blog\controllers;

use Carbon\Carbon;
use singletonn\blog\EventPageView;
use singletonn\blog\models\BlogComments;
use singletonn\blog\models\BlogPosts;
use singletonn\blog\models\BlogPostsSearch;
use singletonn\blog\Module;
use Yii;
use yii\web\NotFoundHttpException;

class PostsController extends Controller
{
    /* @var Module */
    public $module;

    public function actionComment($id)
    {
        $model = BlogPosts::find()
            ->active()
            ->visible()
            ->where(['=', 'id', $id]);
        if (!$model) {
            throw new NotFoundHttpException();
        }
        $modelComment = new BlogComments();
        $modelComment->load(Yii::$app->request->post());
        $modelComment->saveComment();
    }

    public function actionIndex($year = null, $month = null, $day = null)
    {
        $searchModel = new BlogPostsSearch();
        if ($year) {
            $date = Carbon::create($year, $month ?: 1, $day ?: 1, 0, 0);
            $searchModel->startDate = $date->format('Y-m-d H:i:s');
            $stopDate = $date;
            if ($day) {
                $stopDate = $date->addDay();
            }
            if ($month && !$day) {
                $stopDate = $date->addMonth();
            }
            if (!$month && !$day) {
                $stopDate = $date->addYear();
            }
            $searchModel->stopDate = $stopDate->format('Y-m-d H:i:s');
        }
        $dataProvider = $searchModel->searchFrontend();
        if (Yii::$app->request->isAjax) {
            return $this->renderPartial('index', [
                'dataProvider' => $dataProvider,
            ]);
        } else {
            return $this->render('index', [
                'dataProvider' => $dataProvider,
            ]);
        }
    }

    public function actionView($alias)
    {
        $model = BlogPosts::find()
            ->active()
            ->visible()
            ->andWhere(['=', 'full_alias', $alias])
            ->one();
        if ($model === null) {
            throw new NotFoundHttpException();
        }
        $model->updateAttributes([
            'views' => ++$model->views,
        ]);
        $modelComment = new BlogComments();
        $modelComment->post_id = $model->id;
        $this->view->title = $model->title;
        $this->module->trigger($this->module::EVENT_PAGE_VIEW, new EventPageView(['model' => $model]));
        return $this->render('view', [
            'model' => $model,
            'modelComment' => $modelComment,
        ]);
    }

    /**
     * Finds the BlogCategories model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return BlogPosts
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = BlogPosts::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
