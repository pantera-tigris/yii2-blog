<?php

namespace singletonn\blog\controllers;

use singletonn\blog\models\BlogSubscribers;
use Yii;
use yii\web\Response;

/**
 * SubscribersController implements the CRUD actions for BlogSubscribers model.
 */
class SubscribeController extends Controller
{

    public function actionIndex()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $model = new BlogSubscribers();
        $model->load(Yii::$app->request->post(), '');
        if ($model->save()) {
            return ['status' => 'success', 'message' => 'Спасибо за подписку'];
        } else {
            $result = ['status' => 'error'];
            $result['message'] = !empty($model->getErrors()) ? current($model->getErrors())[0] : 'Что то пошло не так!';
            return $result;
        }
    }
}
