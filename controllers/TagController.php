<?php

namespace singletonn\blog\controllers;

use singletonn\blog\EventTagView;
use singletonn\blog\models\BlogPostsSearch;
use singletonn\blog\models\BlogTags;
use singletonn\blog\Module;
use Yii;
use yii\web\NotFoundHttpException;

class TagController extends Controller
{
    /* @var Module */
    public $module;

    public function actionView($alias)
    {
        $model = BlogTags::find()
            ->where(['=', 'name', $alias])
            ->one();
        if ($model === null) {
            throw new NotFoundHttpException();
        }
        $searchModel = new BlogPostsSearch();
        $searchModel->tag = $alias;
        $dataProvider = $searchModel->searchFrontend(Yii::$app->request->queryParams);
        $this->view->title = $model->name;
        $this->module->trigger($this->module::EVENT_TAG_VIEW, new EventTagView(['model' => $model]));
        return $this->render('view', [
            'model' => $model,
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }
}
