<?php

namespace singletonn\blog\controllers;

use singletonn\blog\EventCategoryView;
use singletonn\blog\models\BlogCategories;
use singletonn\blog\models\BlogPostsSearch;
use singletonn\blog\Module;
use Yii;
use yii\web\NotFoundHttpException;

class CategoryController extends Controller
{
    /* @var Module */
    public $module;

    public function actionView($alias)
    {
        $model = BlogCategories::find()
            ->where(['=', 'alias', $alias])
            ->one();
        if ($model === null) {
            throw new NotFoundHttpException();
        }
        $searchModel = new BlogPostsSearch();
        $searchModel->category = $model->id;
        $dataProvider = $searchModel->searchFrontend(Yii::$app->request->queryParams);
        $this->view->title = $model->title;
        $this->module->trigger($this->module::EVENT_CATEGORY_VIEW, new EventCategoryView(['model' => $model]));
        return $this->render('view', [
            'model' => $model,
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Finds the BlogCategories model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return BlogCategories the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = BlogCategories::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
