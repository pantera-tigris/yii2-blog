<?php

namespace singletonn\blog\models;

use yii\behaviors\TimestampBehavior;
use yii\db\Expression;

/**
 * This is the model class for table "blog_subscribers".
 *
 * @property integer $id
 * @property string $email
 * @property string $created_at
 * @property string $updated_at
 */
class BlogSubscribers extends \yii\db\ActiveRecord
{

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'blog_subscribers';
    }

    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::className(),
                'value' => new Expression('NOW()'),
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['email'], 'required'],
            [['email'], 'email', 'message' => 'Некорректный E-mail адрес'],
            [['email'], 'unique', 'message' => 'Вы уже подписаны на рассылку'],
            [['created_at', 'updated_at'], 'safe'],
            [['email'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'email' => 'E-mail',
            'created_at' => 'Дата подписки',
            'updated_at' => 'Дата обновления',
        ];
    }
}
