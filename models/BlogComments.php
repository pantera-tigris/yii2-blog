<?php

namespace singletonn\blog\models;

use dektrium\user\models\User;
use paulzi\adjacencyList\AdjacencyListBehavior;
use Yii;
use yii\behaviors\TimestampBehavior;
use yii\data\ArrayDataProvider;
use yii\db\ActiveRecord;
use yii\db\Expression;

/**
 * This is the model class for table "blog_comments".
 *
 * @property integer $id
 * @property integer $post_id
 * @property integer $user_id
 * @property string $name
 * @property string $email
 * @property string $comment
 * @property integer $parent_id
 * @property integer $level
 * @property integer $status
 * @property string $created_at
 * @property string $updated_at
 *
 * @property BlogPosts $post
 * @property User $user
 *
 *
 * @method ActiveRecord makeRoot()
 * @method ActiveRecord appendTo($node)
 */
class BlogComments extends \yii\db\ActiveRecord
{

    /**
     * @var null|array|ActiveRecord[] comment children
     */
    protected $children;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'blog_comments';
    }

    public static function getCommentDataProvider($postId)
    {
        $dataProvider = new ArrayDataProvider();
        $dataProvider->allModels = static::getTree($postId);
        return $dataProvider;
    }

    public static function getTree($post_id, $maxLevel = null)
    {
        $query = static::find()
            ->andWhere([
                'AND',
                ['post_id' => $post_id],
                ['=', 'status', 1],
            ]);

        if ($maxLevel > 0) {
            $query->andWhere(['<=', 'level', $maxLevel]);
        }

        $models = $query->all();

        if (!empty($models)) {
            $models = self::buildTree($models);
        }

        return $models;
    }

    protected static function buildTree(&$data, $rootID = 0)
    {
        $tree = [];

        foreach ($data as $id => $node) {
            if ($node->parent_id == $rootID) {
                unset($data[$id]);
                $node->children = self::buildTree($data, $node->id);
                $tree[] = $node;
            }
        }

        return $tree;
    }

    /**
     * @return array|null|ActiveRecord[]
     */
    public function getChildren()
    {
        return $this->children;
    }

    /**
     * @param array|null|ActiveRecord[] $children
     */
    public function setChildren($children)
    {
        $this->children = $children;
    }

    /**
     * @return bool
     */
    public function hasChildren()
    {
        return !empty($this->children);
    }

    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::class,
                'value' => new Expression('NOW()'),
            ],
            'adjacencyList' => [
                'class' => AdjacencyListBehavior::class,
                'parentAttribute' => 'parent_id',
                'sortable' => false,
            ],
        ];
    }

    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            if ($this->parent_id > 0) {
                $parentNodeLevel = static::find()->select('level')->where(['id' => $this->parent_id])->scalar();
                $this->level = $parentNodeLevel + 1;
            }
            if (!Yii::$app->user->isGuest) {
                $this->user_id = Yii::$app->user->id;
            }

            return true;
        } else {
            return false;
        }
    }

    public function saveComment()
    {
        if ($this->validate()) {
            if (empty($this->parent_id)) {
                return $this->makeRoot()->save();
            } else {
                $parentComment = static::findOne(['id' => $this->parent_id]);

                return $this->appendTo($parentComment)->save();
            }
        }

        return false;
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['post_id', 'name', 'comment'], 'required'],
            [['post_id', 'user_id', 'parent_id', 'level', 'status'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['email'], 'string', 'max' => 255],
            [['comment'], 'string'],
            [['email'], 'email'],
            [['post_id'], 'exist', 'skipOnError' => true, 'targetClass' => BlogPosts::className(), 'targetAttribute' => ['post_id' => 'id']],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
            [['name', 'email', 'comment'], 'filter', 'filter' => '\yii\helpers\HtmlPurifier::process'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'post_id' => 'Пост',
            'user_id' => 'Пользователь',
            'name' => 'Имя',
            'email' => 'E-mail',
            'comment' => 'Комментарий',
            'parent_id' => 'Parent ID',
            'level' => 'Level',
            'status' => 'Статус',
            'created_at' => 'Дата создания',
            'updated_at' => 'Дата обновления',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPost()
    {
        return $this->hasOne(BlogPosts::className(), ['id' => 'post_id']);
    }

    /**
     * @return string
     */
    public function getContent()
    {
        return nl2br($this->comment);
    }

    public function getAvatar()
    {
        return 'http://www.gravatar.com/avatar?d=mm&f=y&s=50';
    }

    /**
     * @return string
     */
    public function getPostedDate()
    {
        return Yii::$app->formatter->asRelativeTime($this->created_at);
    }

    /**
     * @return string
     */
    public function getAnchorUrl()
    {
        return "#comment-{$this->id}";
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }
}
