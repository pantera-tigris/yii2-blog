<?php

namespace singletonn\blog\models;

use yii\helpers\ArrayHelper;
use yii\helpers\Url;

/**
 * This is the model class for table "blog_tags".
 *
 * @property integer $id
 * @property string $name
 * @property integer $frequency
 *
 * @property BlogPosts[] $posts
 * @property string $url
 */
class BlogTags extends \yii\db\ActiveRecord
{
    /**
     * @param bool $map
     * @return array|\yii\db\ActiveRecord[]|BlogTags[]
     */
    public static function getList($map = true)
    {
        $models = self::find()->all();
        if ($map) {
            return ArrayHelper::map($models, 'name', 'name');
        }
        return $models;
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'blog_tags';
    }

    public function getUrl()
    {
        return Url::to(['/blog/tag/view', 'alias' => $this->name]);
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['frequency'], 'integer'],
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Имя',
            'frequency' => 'Частота',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPosts()
    {
        return $this->hasMany(BlogPosts::className(), ['id' => 'post_id'])->viaTable('blog_post_tag_assn', ['tag_id' => 'id']);
    }
}
