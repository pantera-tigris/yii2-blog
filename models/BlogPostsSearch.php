<?php

namespace singletonn\blog\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * BlogPostsSearch represents the model behind the search form about `common\modules\blog\models\BlogPosts`.
 */
class BlogPostsSearch extends BlogPosts
{

    public $category;
    public $tag;
    public $startDate;
    public $stopDate;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id'], 'integer'],
            [['title', 'description_short', 'description', 'image', 'created_at', 'updated_at', 'category', 'tag', 'startDate', 'stopDate'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    public function searchFrontend($params = [])
    {
        $query = BlogPosts::find()
            ->active()
            ->visible()
            ->joinWith(['categories', 'tags'])
            ->groupBy('blog_posts.id');

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 10,
            ],
            'sort' => [
                'defaultOrder' => ['published_at' => SORT_DESC]
            ],
        ]);

        $this->load($params);

        // grid filtering conditions
        $query->andFilterWhere([
            'blog_categories.id' => $this->category,
            'name' => $this->tag,
        ]);
        $query->andFilterWhere(['between', 'published_at', $this->startDate, $this->stopDate]);

        return $dataProvider;
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = BlogPosts::find()
            ->joinWith(['categories', 'tags']);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' => ['id' => SORT_DESC]
            ],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'blog_posts.id' => $this->id,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'category_id' => $this->category,
            'name' => $this->tag,
        ]);

        $query->andFilterWhere(['like', 'blog_posts.title', $this->title])
            ->andFilterWhere(['like', 'description_short', $this->description_short])
            ->andFilterWhere(['like', 'description', $this->description])
            ->andFilterWhere(['like', 'image', $this->image]);

        return $dataProvider;
    }
}
