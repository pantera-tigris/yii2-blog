<?php

namespace singletonn\blog\models;

use yii\behaviors\TimestampBehavior;
use yii\db\Expression;
use yii\helpers\ArrayHelper;
use yii\helpers\StringHelper;
use yii\helpers\Url;

/**
 * This is the model class for table "blog_categories".
 *
 * @property integer $id
 * @property string $title
 * @property string $created_at
 * @property string $updated_at
 * @property string $alias
 *
 * @property BlogCategoryPost[] $blogCategoryPosts
 * @property BlogPosts[] $posts
 * @property string $url
 */
class BlogCategories extends \yii\db\ActiveRecord
{
    /**
     * @param bool $map
     * @return array|\yii\db\ActiveRecord[]|BlogCategories[]
     */
    public static function getList($map = true)
    {
        $models = self::find()->all();
        if ($map) {
            return ArrayHelper::map($models, 'id', 'title');
        }
        return $models;
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'blog_categories';
    }

    public function getUrl()
    {
        return Url::to(['/blog/category/view', 'alias' => $this->alias]);
    }

    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'value' => new Expression('NOW()'),
            ],
            'slug' => [
                'class' => 'Zelenin\yii\behaviors\Slug',
                'slugAttribute' => 'alias',
                'attribute' => 'title',
                'immutable' => false,
            ]
        ];
    }

    public function afterSave($insert, $changedAttributes)
    {
        BlogAlias::add(StringHelper::basename($this::className()), $this->id, $this->alias);
        parent::afterSave($insert, $changedAttributes);
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title'], 'required'],
            [['created_at', 'updated_at'], 'safe'],
            [['title'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Заголовок',
            'created_at' => 'Дата создания',
            'updated_at' => 'Дата обовления',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBlogCategoryPosts()
    {
        return $this->hasMany(BlogCategoryPost::className(), ['category_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPosts()
    {
        return $this->hasMany(BlogPosts::className(), ['id' => 'post_id'])->viaTable('blog_category_post', ['category_id' => 'id']);
    }
}
