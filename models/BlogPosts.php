<?php

namespace singletonn\blog\models;

use Carbon\Carbon;
use creocoder\taggable\TaggableBehavior;
use himiklab\thumbnail\EasyThumbnailImage;
use voskobovich\behaviors\ManyToManyBehavior;
use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;
use yii\helpers\StringHelper;
use yii\helpers\Url;
use yii\web\UploadedFile;

/**
 * This is the model class for table "blog_posts".
 *
 * @property integer $id
 * @property string $title
 * @property string $description_short
 * @property string $description
 * @property string $image
 * @property string $created_at
 * @property string $updated_at
 * @property string $published_at
 * @property integer $status
 * @property integer $views
 * @property string $alias
 * @property string $full_alias
 * @property integer $main
 *
 * @property BlogCategoryPost[] $blogCategoryPosts
 * @property BlogCategories[] $categories
 * @property BlogTags[] $tags
 * @property string $url
 */
class BlogPosts extends \yii\db\ActiveRecord
{

    public $mainImage;

    /**
     * @return array|null|\yii\db\ActiveRecord|BlogPosts
     */
    public static function getMainPost()
    {
        return self::find()
            ->orderBy(['main' => SORT_DESC, 'id' => SORT_DESC])
            ->one();
    }

    public static function find()
    {
        return new BlogPostsQuery(get_called_class());
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'blog_posts';
    }

    public function getCommentCount()
    {
        return BlogComments::find()
            ->where(['=', 'post_id', $this->id])
            ->count();
    }

    /**
     * @return BlogPosts[]
     */
    public function getRelatedPosts()
    {
        return BlogPosts::find()
            ->joinWith(['categories'])
//            ->where(['IN', 'category_id', ArrayHelper::getColumn($this->categories, 'id')])
//            ->active()
//            ->visible()
            ->groupBy('blog_posts.id')
            ->limit(4)
            ->all();
    }

    public function getUrl()
    {
        return Url::to(['/blog/posts/view', 'alias' => $this->full_alias]);
    }

    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'value' => new Expression('NOW()'),
            ],
            [
                'class' => ManyToManyBehavior::className(),
                'relations' => [
                    'category_ids' => 'categories',
                ],
            ],
            'taggable' => [
                'class' => TaggableBehavior::className(),
                'tagValuesAsArray' => true,
            ],
            'slug' => [
                'class' => 'Zelenin\yii\behaviors\Slug',
                'slugAttribute' => 'alias',
                'attribute' => 'title',
                'immutable' => false,
            ]
        ];
    }

    public function image($width = null, $height = null, $type = EasyThumbnailImage::THUMBNAIL_INSET)
    {
        if (!file_exists(Yii::$app->getModule('blog')->mainImagePath . $this->image)) {
            return '';
        }
        if (!$width || !$height) {
            return Yii::$app->getModule('blog')->mainImageUrl . $this->image;
        }
        return EasyThumbnailImage::thumbnailFileUrl(
            Yii::$app->getModule('blog')->mainImagePath . $this->image,
            $width,
            $height,
            $type,
            Yii::$app->getModule('blog')->quality
        );
    }

    public function beforeSave($insert)
    {
        $this->published_at = date('Y-m-d H:i:s', strtotime($this->published_at));
        $this->mainImage = UploadedFile::getInstance($this, 'mainImage');
        if ($this->mainImage) {
            $name = uniqid('', true) . '.' . $this->mainImage->extension;
            $this->image = $name;
            $this->mainImage->saveAs(Yii::$app->getModule('blog')->mainImagePath . $name);
        }
        $published_at = new Carbon();
        $published_at->setTimestamp(strtotime($this->published_at));
        $this->full_alias = $published_at->year . '/' . $published_at->format('m') . '/' . $published_at->format('d') . '/' . $this->alias;
        return parent::beforeSave($insert);
    }

    public function afterSave($insert, $changedAttributes)
    {
        BlogAlias::add(StringHelper::basename($this::className()), $this->id, $this->full_alias);
        parent::afterSave($insert, $changedAttributes);
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title', 'status', 'published_at', 'category_ids'], 'required'],
            [['status', 'views', 'main'], 'integer'],
            [['description_short', 'description', 'alias', 'full_alias'], 'string'],
            [['created_at', 'updated_at', 'tagValues'], 'safe'],
            [['category_ids'], 'each', 'rule' => ['integer']],
            ['mainImage', 'file', 'extensions' => 'png, jpg'],
            [['title', 'image'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Заголовок',
            'description_short' => 'Краткое описание',
            'description' => 'Описание',
            'image' => 'Главная картинка',
            'mainImage' => 'Главная картинка',
            'created_at' => 'Дата создания',
            'updated_at' => 'Дата изменения',
            'published_at' => 'Дата публикации',
            'category_ids' => 'Категория',
            'tagValues' => 'Теги',
            'status' => 'Статус',
            'main' => 'Показать на главной',
        ];
    }

    public function getTags()
    {
        return $this->hasMany(BlogTags::className(), ['id' => 'tag_id'])
            ->viaTable('blog_post_tag_assn', ['post_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBlogCategoryPosts()
    {
        return $this->hasMany(BlogCategoryPost::className(), ['post_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategories()
    {
        return $this->hasMany(BlogCategories::className(), ['id' => 'category_id'])->viaTable('blog_category_post', ['post_id' => 'id']);
    }
}
