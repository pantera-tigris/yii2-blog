<?php

namespace singletonn\blog\models;

use yii\db\Expression;

class BlogPostsQuery extends \yii\db\ActiveQuery
{
    /**
     * @return BlogPostsQuery
     */
    public function active()
    {
        return $this->andWhere('[[status]]=1');
    }

    /**
     * @return BlogPostsQuery
     */
    public function visible()
    {
        return $this->andWhere(['<=', 'published_at', new Expression('NOW()')]);
    }

    public function all($db = null)
    {
        return parent::all($db);
    }

    public function one($db = null)
    {
        return parent::one($db);
    }
}
