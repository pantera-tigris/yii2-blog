<?php

namespace singletonn\blog\models;

/**
 * This is the model class for table "blog_alias".
 *
 * @property string $model
 * @property integer $model_id
 * @property string $alias
 */
class BlogAlias extends \yii\db\ActiveRecord
{

    public static function add($model, $model_id, $alias)
    {
        if (!self::check($model, $model_id, $alias)) {
            $self = new self;
            $self->model = $model;
            $self->model_id = $model_id;
            $self->alias = $alias;
            $self->save();
        }
    }

    public static function check($model, $model_id, $alias)
    {
        return BlogAlias::find()
            ->where([
                'AND',
                ['=', 'model', $model],
                ['=', 'model_id', $model_id],
                ['=', 'alias', $alias],
            ])
            ->count();
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'blog_alias';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['model', 'model_id', 'alias'], 'required'],
            [['model_id'], 'integer'],
            [['alias'], 'string'],
            [['model'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'model' => 'Model',
            'model_id' => 'Model ID',
            'alias' => 'Alias',
        ];
    }
}
