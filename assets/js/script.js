var blogMasonry;
$(window).on('load', function () {
    blogMasonry = $('#posts-container .blog-post-items').masonry({
        itemSelector: '.post',
    });
});

$(function () {

    var commentForm = $('#blog-comments-form');
    if (commentForm.length) {
//Возврошение формы комента на место (отмена ответа)
        $(document).on('click', '#cancel-comment-reply-link', function () {
            $('#blog-comments').after(commentForm);
            commentForm.find('#blogcomments-parent_id').val('');
            commentForm.find('#cancel-comment-reply-link').hide();
            return false;
        });

//Перемишение формы комента для ответа на другой комент
        $(document).on('click', '.comment-reply-link', function () {
            var self = $(this);
            var commentId = self.data('comment-id');
            self.parents('li').after(commentForm);
            commentForm.find('#blogcomments-parent_id').val(commentId);
            commentForm.find('#cancel-comment-reply-link').show();
            return false;
        });

//Отправка формы комента и перезагрузка pjax контейнера
        $(document).on('submit', '#commentform', function () {
            var self = $(this);
            var submitBtn = self.find('input[type="submit"]');
            if (submitBtn.prop('disabled')) {
                return false;
            }
            submitBtn.prop('disabled', true);
            $.ajax({
                url: self.attr('action'),
                data: self.serialize(),
                type: 'POST',
                success: function () {
                    $.pjax.reload('#blog-comments');
                    self.find('#comment-input input').val('');
                    self.find('#comment-textarea textarea').val('');
                    if ($('#blog-comments').next('#blog-comments-form').length === 0) {
                        $('#blog-comments').after(commentForm);
                        commentForm.find('#blogcomments-parent_id').val('');
                        commentForm.find('#cancel-comment-reply-link').hide();
                    }
                },
                complete: function () {
                    submitBtn.prop('disabled', false);
                }
            });
            return false;
        });
    }

//Подписка на новости
    $('#emailSub-form').submit(function () {
        var form = $(this);
        var btn = form.find('.submit');
        var resultMsg = form.next();
        resultMsg.removeClass('success, error');
        resultMsg.hide();
        btn.prop('disabled', true);
        $.ajax({
            url: form.attr('action'),
            type: 'POST',
            dataType: 'json',
            data: form.serialize(),
            success: function (result) {
                if (result.status === 'success') {
                    resultMsg.text(result.message).addClass('success');
                } else {
                    resultMsg.text(result.message);
                    resultMsg.addClass('error');
                }
                form.find('input[type=text]').val('');
            },
            complete: function () {
                btn.prop('disabled', false);
                resultMsg.show();
            }
        });
        return false;
    });
});