<?php

namespace singletonn\blog\components;

use PDO;
use Yii;
use yii\web\UrlRuleInterface;

class UrlManager implements UrlRuleInterface
{
    public function parseRequest($manager, $request)
    {
        if (strpos($request->getPathInfo(), Yii::$app->getModule('blog')->urlPrefix) === false) {
            return false;
        }
        if (Yii::$app->getModule('blog')->urlPrefix . Yii::$app->getModule('blog')->urlSuffix === $request->getPathInfo()) {
            return [Yii::$app->getModule('blog')->urlPrefix . '/posts/index', []];
        }
        $path = trim(str_replace(Yii::$app->getModule('blog')->urlPrefix, '', $request->getPathInfo()), '/');
        if (preg_match('#^(\d{4})/?(\d{2})?/?(\d{2})?$#', $path, $matches)) {
            unset($matches[0]);
            $keys = ['year', 'month', 'day'];
            $count = min(count($keys), count($matches));
            $params = array_combine(array_slice($keys, 0, $count), array_slice($matches, 0, $count));
            return [Yii::$app->getModule('blog')->urlPrefix . '/posts/index', $params];
        }
        $sql = 'SELECT * FROM blog_alias WHERE alias = :alias';
        $command = Yii::$app->db->createCommand($sql);
        $command->bindValue(':alias', $path, PDO::PARAM_STR);
        $res = $command->queryOne();
        if ($res) {
            if ($res['model'] == 'BlogCategories') {
                return [Yii::$app->getModule('blog')->urlPrefix . '/category/view', [
                    'alias' => $res['alias'],
                ]];
            } elseif ($res['model'] == 'BlogPosts') {
                return [Yii::$app->getModule('blog')->urlPrefix . '/posts/view', [
                    'alias' => $res['alias'],
                ]];
            }
            return false;
        }
        return false;
    }

    /**
     * Creates a URL according to the given route and parameters.
     * @param \yii\web\UrlManager $manager the URL manager
     * @param string $route the route. It should not have slashes at the beginning or the end.
     * @param array $params the parameters
     * @return string|boolean the created URL, or false if this rule cannot be used for creating this URL.
     */
    public function createUrl($manager, $route, $params)
    {
        if ($route === 'blog/category/view') {
            $url = 'blog/';
            if (array_key_exists('alias', $params) && !empty($params['alias'])) {
                $url .= $params['alias'];
            }
            return $url;
        } elseif ($route === 'blog/posts/view') {
            $url = 'blog/';
            $url .= $params['alias'];
            return $url;
        }
        return false;
    }
}