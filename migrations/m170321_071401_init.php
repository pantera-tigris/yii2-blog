<?php

use yii\db\Migration;

class m170321_071401_init extends Migration
{
    public function up()
    {
        $this->createTable('blog_categories', [
            'id' => $this->primaryKey(),
            'title' => $this->string()->notNull(),
            'created_at' => $this->timestamp()->notNull()->defaultExpression('CURRENT_TIMESTAMP'),
            'updated_at' => $this->timestamp()->notNull()->defaultValue('0000-00-00 00:00:00'),
        ]);

        $this->createTable('blog_posts', [
            'id' => $this->primaryKey(),
            'title' => $this->string()->notNull(),
            'description_short' => $this->text(),
            'description' => $this->text(),
            'image' => $this->string(),
            'created_at' => $this->timestamp()->notNull()->defaultExpression('CURRENT_TIMESTAMP'),
            'updated_at' => $this->timestamp()->notNull()->defaultValue('0000-00-00 00:00:00'),
        ]);

        $this->createTable('blog_category_post', [
            'category_id' => $this->integer()->notNull(),
            'post_id' => $this->integer()->notNull(),
        ]);

        $this->addPrimaryKey('blog_category_post_pk', 'blog_category_post', [
            'category_id',
            'post_id',
        ]);

        $this->createIndex('blog_category_post-category_id', 'blog_category_post', 'category_id');
        $this->createIndex('blog_category_post-post_id', 'blog_category_post', 'post_id');
        $this->addForeignKey('blog_category_post-category_id:blog_categories-id', 'blog_category_post', 'category_id', 'blog_categories', 'id', 'CASCADE', 'CASCADE');
        $this->addForeignKey('blog_category_post-post_id:blog_posts-id', 'blog_category_post', 'post_id', 'blog_posts', 'id', 'CASCADE', 'CASCADE');
    }

    public function down()
    {
        return true;
    }
}
