<?php

use yii\db\Migration;

/**
 * Handles the creation of table `blog_subscribers`.
 */
class m170417_001702_create_blog_subscribers_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('blog_subscribers', [
            'id' => $this->primaryKey(),
            'email' => $this->string()->notNull(),
            'created_at' => $this->timestamp()->notNull()->defaultExpression('CURRENT_TIMESTAMP'),
            'updated_at' => $this->timestamp()->null(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('blog_subscribers');
    }
}
