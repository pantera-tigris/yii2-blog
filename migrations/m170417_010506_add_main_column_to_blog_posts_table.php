<?php

use yii\db\Migration;

/**
 * Handles adding main to table `blog_posts`.
 */
class m170417_010506_add_main_column_to_blog_posts_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('blog_posts', 'main', $this->boolean()->notNull()->defaultValue(0));
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('blog_posts', 'main');
    }
}
