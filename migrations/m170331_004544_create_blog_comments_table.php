<?php

use yii\db\Migration;

/**
 * Handles the creation of table `blog_comments`.
 */
class m170331_004544_create_blog_comments_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('blog_comments', [
            'id' => $this->primaryKey(),
            'post_id' => $this->integer()->notNull(),
            'user_id' => $this->integer()->null(),
            'name' => $this->string()->notNull(),
            'email' => $this->string()->null(),
            'comment' => $this->string()->notNull(),
            'parent_id' => $this->integer()->null(),
            'level' => $this->smallInteger()->notNull()->defaultValue(1),
            'status' => $this->boolean()->notNull()->defaultValue(1),
            'created_at' => $this->timestamp()->notNull()->defaultExpression('CURRENT_TIMESTAMP'),
            'updated_at' => $this->timestamp()->notNull()->defaultValue('0000-00-00 00:00:00')
        ]);

        $this->createIndex('blog_comments-user_id', 'blog_comments', 'user_id');
        $this->addForeignKey('blog_comments-user_id:user-id', 'blog_comments', 'user_id', 'user', 'id', 'CASCADE', 'CASCADE');
        $this->createIndex('blog_comments-post_id', 'blog_comments', 'post_id');
        $this->addForeignKey('blog_comments-post_id:blog_posts-id', 'blog_comments', 'post_id', 'blog_posts', 'id', 'CASCADE', 'CASCADE');
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('blog_comments');
    }
}
