<?php

use yii\db\Migration;

class m170327_022924_post_add_full_alias extends Migration
{
    public function up()
    {
        $this->addColumn('blog_posts', 'full_alias', $this->string());
    }

    public function down()
    {
        $this->dropColumn('blog_posts', 'full_alias');
        return true;
    }
}
