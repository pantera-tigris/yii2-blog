<?php

use yii\db\Migration;

class m170323_040330_add_alias extends Migration
{
    public function up()
    {
        $this->addColumn('blog_categories', 'alias', $this->string()->notNull());
        $this->addColumn('blog_posts', 'alias', $this->string()->notNull());
        $this->addColumn('blog_tags', 'alias', $this->string()->notNull());

        $this->createTable('blog_alias', [
            'model' => $this->string()->notNull(),
            'model_id' => $this->integer()->notNull(),
            'alias' => $this->text()->notNull(),
        ]);
    }

    public function down()
    {
        $this->dropColumn('blog_categories', 'alias');
        $this->dropColumn('blog_posts', 'alias');
        $this->dropColumn('blog_tags', 'alias');
        $this->dropTable('blog_alias');
        echo "m170323_040330_add_alias cannot be reverted.\n";

        return true;
    }
}
