<?php

use yii\db\Migration;

class m170322_042937_update_posts extends Migration
{
    public function up()
    {
        $this->addColumn('blog_posts', 'published_at', $this->timestamp()->notNull());
        $this->addColumn('blog_posts', 'status', $this->boolean()->notNull()->defaultValue(1));
    }

    public function down()
    {
        $this->dropColumn('blog_posts', 'published_at');
        $this->dropColumn('blog_posts', 'status');
        echo "m170322_042937_update_posts cannot be reverted.\n";
        return true;
    }
}
