<?php

use yii\db\Migration;

class m170531_030110_drop_tag_alias extends Migration
{
    public function up()
    {
        $this->dropColumn('blog_tags', 'alias');
    }

    public function down()
    {
        $this->addColumn('blog_tags', 'alias', $this->string()->notNull());
        return true;
    }
}
