<?php

use yii\db\Migration;
use yii\db\Schema;

/**
 * Handles the creation of table `tags`.
 */
class m170322_033051_create_tags_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('blog_tags', [
            'id' => $this->primaryKey(),
            'name' => Schema::TYPE_STRING . ' NOT NULL',
            'frequency' => Schema::TYPE_INTEGER . ' NOT NULL DEFAULT 0',
        ]);

        $this->createTable('blog_post_tag_assn', [
            'post_id' => Schema::TYPE_INTEGER . ' NOT NULL',
            'tag_id' => Schema::TYPE_INTEGER . ' NOT NULL',
        ]);

        $this->addPrimaryKey('', 'blog_post_tag_assn', ['post_id', 'tag_id']);

        $this->addForeignKey('blog_post_tag_assn-post_id:blog_posts-id', 'blog_post_tag_assn', 'post_id', 'blog_posts', 'id', 'CASCADE', 'CASCADE');
        $this->addForeignKey('blog_post_tag_assn-tag_id:blog_tags-id', 'blog_post_tag_assn', 'tag_id', 'blog_tags', 'id', 'CASCADE', 'CASCADE');
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('blog_post_tag_assn');
        $this->dropTable('blog_tags');
    }
}
