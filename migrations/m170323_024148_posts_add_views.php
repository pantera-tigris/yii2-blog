<?php

use yii\db\Migration;

class m170323_024148_posts_add_views extends Migration
{
    public function up()
    {
        $this->addColumn('blog_posts', 'views', $this->integer()->null()->defaultValue(0));
    }

    public function down()
    {
        echo "m170323_024148_posts_add_views cannot be reverted.\n";

        return true;
    }
}
