<?php

use kartik\widgets\Select2;
use mihaildev\ckeditor\CKEditor;
use singletonn\blog\models\BlogCategories;
use singletonn\blog\models\BlogTags;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model singletonn\blog\models\BlogPosts */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="blog-posts-form">

    <?php $form = ActiveForm::begin([
        'options' => [
            'enctype' => 'multipart/form-data'
        ]
    ]); ?>

    <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'category_ids')->widget(Select2::className(), [
        'data' => BlogCategories::getList(),
        'options' => [
            'placeholder' => 'Выберите категорию',
            'multiple' => true
        ],
    ]) ?>

    <?= $form->field($model, 'tagValues')->widget(Select2::className(), [
        'data' => BlogTags::getList(),
        'options' => ['placeholder' => 'Выберите теги', 'multiple' => true],
        'pluginOptions' => [
            'tags' => true,
            'tokenSeparators' => [','],
        ],
    ]) ?>

    <?= $form->field($model, 'description_short')->widget(CKEditor::className(), [
        'editorOptions' => \mihaildev\elfinder\ElFinder::ckeditorOptions(['elfinder-blog'], [
            'preset' => 'basic',
            'height' => 200
        ]),
    ]); ?>

    <?= $form->field($model, 'description')->widget(CKEditor::className(), [
        'editorOptions' => \mihaildev\elfinder\ElFinder::ckeditorOptions(['elfinder-blog'], [
            'preset' => 'full',
            'height' => 200
        ]),
    ]); ?>

    <?php if ($model->image): ?>
        <?= Html::img($model->image(300, 300)) ?>
    <?php endif; ?>

    <?= $form->field($model, 'mainImage')->fileInput() ?>

    <?= $form->field($model, 'main')->checkbox() ?>

    <?= $form->field($model, 'status')->checkbox() ?>

    <?= $form->field($model, 'published_at')->widget(\nkovacs\datetimepicker\DateTimePicker::className(), [
        'clientOptions' => [
            'extraFormats' => ['YYYY-MM-DD HH:mm'],
        ],
    ]) ?>

    <div class="form-group">
        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
