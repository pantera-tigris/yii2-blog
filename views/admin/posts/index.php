<?php

use kartik\widgets\Select2;
use singletonn\blog\models\BlogCategories;
use singletonn\blog\models\BlogTags;
use yii\grid\GridView;
use yii\helpers\Html;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel singletonn\blog\models\BlogPostsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Посты';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="blog-posts-index">

    <h1><?= Html::encode($this->title) ?><?= Html::a('<i class="fa fa-plus"></i>Добавить пост', ['create'], ['class' => 'btn btn-success pull-right']) ?></h1>


    <?php Pjax::begin(); ?>    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            'id',
            [
                'attribute' => 'mainImage',
                'format' => 'raw',
                'value' => function ($model) {
                    if ($model->image) {
                        return Html::a(Html::img($model->image(70, 70, \himiklab\thumbnail\EasyThumbnailImage::THUMBNAIL_OUTBOUND)), ['update', 'id' => $model->id], [
                            'data-pjax' => 0
                        ]);
                    }
                }
            ],
            [
                'attribute' => 'title',
                'format' => 'raw',
                'value' => function ($model) {
                    return Html::a($model->title, ['update', 'id' => $model->id], [
                        'data-pjax' => 0
                    ]);
                }
            ],
            [
                'attribute' => 'category_ids',
                'format' => 'raw',
                'value' => function ($model) {
                    $categories = [];
                    foreach ($model->categories as $category) {
                        $categories[] = $category->title;
                    }
                    return implode(', ', $categories);
                },
                'filter' => Select2::widget([
                    'model' => $searchModel,
                    'attribute' => 'category',
                    'data' => BlogCategories::getList(),
                    'pluginOptions' => [
                        'allowClear' => true,
                        'placeholder' => 'Категория'
                    ],
                ]),
            ],
            [
                'attribute' => 'tagValues',
                'format' => 'raw',
                'value' => function ($model) {
                    return $model->getTagValues(false);
                },
                'filter' => Select2::widget([
                    'model' => $searchModel,
                    'attribute' => 'tag',
                    'data' => BlogTags::getList(),
                    'pluginOptions' => [
                        'allowClear' => true,
                        'placeholder' => 'Тег'
                    ],
                ]),
            ],
            'published_at:datetime',

            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{update}{delete}'
            ],
        ],
    ]); ?>
    <?php Pjax::end(); ?></div>
