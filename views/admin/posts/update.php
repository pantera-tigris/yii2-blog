<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model singletonn\blog\models\BlogPosts */

$this->title = 'Обновить пост: ' . $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Посты', 'url' => ['index']];
$this->params['breadcrumbs'][] = $model->title;
?>
<div class="blog-posts-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
