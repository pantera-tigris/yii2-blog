<?php

use yii\grid\GridView;
use yii\helpers\Html;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel singletonn\blog\models\BlogSubscribersSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Подписчики';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="blog-subscribers-index">

    <h1><?= Html::encode($this->title) ?><?= Html::a('<i class="fa fa-plus"></i>Добавить подписчика', ['create'], ['class' => 'btn btn-success pull-right']) ?></h1>
    <p>

    </p>
    <?php Pjax::begin(); ?>    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            'id',
            'email:email',
            'created_at',
            'updated_at',
            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{update}{delete}'
            ],
        ],
    ]); ?>
    <?php Pjax::end(); ?></div>
