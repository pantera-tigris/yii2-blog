<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model singletonn\blog\models\BlogSubscribers */

$this->title = 'Добавление подписчика';
$this->params['breadcrumbs'][] = ['label' => 'Подписчики', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="blog-subscribers-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
