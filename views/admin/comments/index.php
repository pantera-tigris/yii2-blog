<?php

use yii\grid\GridView;
use yii\helpers\Html;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel singletonn\blog\models\BlogCommentsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Комментарии';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="blog-comments-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <?php Pjax::begin(); ?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            'id',
            [
                'attribute' => 'post_id',
                'format' => 'raw',
                'value' => function ($model) {
                    return Html::a($model->post->title, ['/blog/admin/posts/update', 'id' => $model->post_id], [
                        'data-pjax' => 0,
                    ]);
                }
            ],
            'name',
            'email',
            'comment',
            'created_at:datetime',
            [
                'attribute' => 'status',
                'format' => 'raw',
                'value' => function ($model) {
                    return '<span class="label label-' . ($model->status ? 'success' : 'danger') . '">' . ($model->status ? 'Опубликован' : 'Скрыт') . '</span>';
                },
                'contentOptions' => [
                    'class' => 'grid-status-column text-center',
                ],
            ],

            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{update}{delete}'
            ],
        ],
    ]); ?>
    <?php Pjax::end(); ?>
</div>
