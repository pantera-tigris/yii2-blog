<?php

use yii\helpers\Html;

/* @var $this \yii\web\View */
/* @var $model singletonn\blog\models\BlogComments */
?>
<li class="comment" id="comment-<?php echo $model->id; ?>">
    <div class="the-comment" data-comment-content-id="<?php echo $model->id ?>">
        <div class="avatar">
            <?php echo Html::img($model->getAvatar(), ['alt' => $model->name]); ?>
        </div>
        <div class="comment-box">
            <div class="comment-author meta">
                <span><?php echo $model->name; ?></span>
                <?php echo Html::a($model->getPostedDate(), $model->getAnchorUrl(), ['class' => 'comment-date']); ?>
                <?php echo Html::a('- Ответить', '#', ['class' => 'comment-reply-link', 'data' => ['action' => 'reply', 'comment-id' => $model->id]]); ?>
            </div>
            <div class="comment-text">
                <?php echo $model->getContent(); ?>
            </div>
        </div>
    </div>
</li>
<?php if ($model->hasChildren()) : ?>
    <ul class="children">
        <?php foreach ($model->getChildren() as $children) : ?>
            <li class="comment" id="comment-<?php echo $children->id; ?>">
                <?php echo $this->render('_comment', ['model' => $children]) ?>
            </li>
        <?php endforeach; ?>
    </ul>
<?php endif; ?>
