<?php

use himiklab\thumbnail\EasyThumbnailImage;
use singletonn\blog\models\BlogComments;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\widgets\ListView;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $model singletonn\blog\models\BlogPosts */
/* @var $modelComment singletonn\blog\models\BlogComments */
$blog = $this->context->module;
?>
<div id="post-1607"
     class="detail-post post post-1607 type-post status-publish format-standard has-post-thumbnail hentry category-stranovedenie tag-puteshestviya tag-rekomendatsii">

    <h1 class="entry-title" data-fontsize="24" data-lineheight="28"><?= $model->title ?></h1>
    <div class="post-meta-con">
        <p class="fusion-single-line-meta">
            <span><?= Yii::$app->formatter->asDate($model->published_at) ?></span>
            <span class="fusion-inline-sep">|</span>
            <?php foreach ($model->categories as $category): ?>
                <a href="<?= $category->url ?>" rel="category tag"><?= $category->title ?></a>
            <?php endforeach; ?>
            <span class="fusion-addinfo">
                <?php if(!empty($blog) && $blog->isCommentsAllowed):?>
                    <span class="fusion-scomment">
                        <i class="fa fa-comments-o" aria-hidden="true"></i> <?= $model->getCommentCount() ?>
                    </span>
                <?php endif;?>
                <span class="fusion-sviews">
                    <i class="fa fa-eye" aria-hidden="true"></i> <?= $model->views ?>
                </span>
              </span>
        </p>
    </div>
    <div class="post-content">
        <?php
        if ($model->description_short) {
            echo $model->description_short;
        }
        if ($model->image) {
            echo Html::tag('p', Html::img($model->image()));
        }
        if ($model->description) {
            echo $model->description;
        }
        ?>
    </div>

    <div class="fusion-meta-info">
        <div class="fusion-meta-info-wrapper">

            <span><?= Yii::$app->formatter->asDate($model->published_at) ?></span>
            <?php foreach ($model->categories as $category): ?>
                <span class="fusion-inline-sep">|</span>
                <a href="<?= $category->url ?>" rel="category tag"><?= $category->title ?></a>
            <?php endforeach; ?>
            <span class="fusion-addinfo">
                <span class="fusion-sviews">
                    <i class="fa fa-eye" aria-hidden="true"></i> <?= $model->views ?>
                </span>
            </span>
        </div>
    </div>
    <?php if ($model->tagValues): ?>
        <div class="ll-post-tag-con">
            <span class="ll-post-tag-text">Теги: </span>
            <?php foreach ($model->tags as $tag): ?>
                <a class="ll-post-tag" href="<?= $tag->url ?>"><?= $tag->name ?></a>
            <?php endforeach; ?>
        </div>
    <?php endif; ?>

    <div class="fusion-sharing-box fusion-single-sharing-box share-box">
        <h4 data-fontsize="13" data-lineheight="19">До чего полезная статья! Расскажу про нее друзьям!</h4>
        <div class="ya-share2" data-services="vkontakte,facebook,odnoklassniki,gplus,twitter,whatsapp,telegram"></div>
    </div>
    <?php if ($model->getRelatedPosts()): ?>
        <div class="related-posts single-related-posts">
            <div class="fusion-title fusion-title-size-three sep-double" style="margin-top:0px;margin-bottom:31px;"><h3
                        class="title-heading-left" data-fontsize="18" data-lineheight="27">Похожие записи</h3>
                <div class="title-sep-container">
                    <div class="title-sep sep-double"></div>
                </div>
            </div>


            <ul class="fusion-carousel-holder clearfix">
                <?php foreach ($model->getRelatedPosts() as $post): ?>
                    <li class="fusion-carousel-item">
                        <div class="fusion-carousel-item-wrapper">
                            <?php if ($post->image): ?>
                                <div class="fusion-image-wrapper fusion-image-size-fixed">
                                    <a href="<?= $post->url ?>">
                                        <?= Html::img($post->image(174, 133, EasyThumbnailImage::THUMBNAIL_OUTBOUND)) ?>
                                    </a>
                                </div>
                            <?php endif; ?>

                            <?php if ($post->description_short): ?>
                                <h4 class="fusion-carousel-title">
                                    <a href="<?= $post->url ?>">
                                        <?= mb_strlen($post->description_short, 'UTF-8') > 100 ? mb_substr($post->description_short, 0, 100) . '...' : $post->description_short ?>
                                    </a>
                                </h4>
                            <?php endif; ?>

                            <div class="fusion-carousel-meta">
                                <span class="fusion-date"><?= Yii::$app->formatter->asDate($post->published_at) ?></span>
                            </div>
                        </div>
                    </li>
                <?php endforeach; ?>
            </ul>
        </div>
    <?php endif; ?>
    <?php if(!empty($blog) && $blog->isCommentsAllowed):?>
        <div id="comments">
            <div class="fusion-title fusion-title-size-three sep-double" style="margin-top:0px;margin-bottom:31px;">
                <h3 class="title-heading-left" data-fontsize="18" data-lineheight="27">
                    <?= $model->getCommentCount() . ' ' . Yii::t('app', '{count, plural, one{комментарий} few{комментария} many{комментариев} other{комментария}}', ['count' => $model->getCommentCount()]); ?>
                </h3>
                <div class="title-sep-container">
                    <div class="title-sep sep-double"></div>
                </div>
            </div>
            <?php Pjax::begin([
                'id' => 'blog-comments'
            ]) ?>
            <?php
            $dataProvider = BlogComments::getCommentDataProvider($model->id);
            if ($dataProvider->totalCount) {
                echo ListView::widget([
                    'dataProvider' => $dataProvider,
                    'layout' => "{items}\n{pager}",
                    'itemView' => '_comment',
                    'options' => [
                        'tag' => 'ol',
                        'class' => 'comment-list commentlist',
                    ],
                    'itemOptions' => [
                        'tag' => false,
                    ],
                ]);
            }
            ?>
            <?php Pjax::end() ?>
            <div id="blog-comments-form">
                <div class="fusion-title title fusion-title-size-three sep-double">
                    <h3 id="reply-title" class="comment-reply-title title-heading-left">
                        Оставить комментарий
                        <small>
                            <a rel="nofollow" id="cancel-comment-reply-link" href="javascript::void(0)"
                               style="display: none;">
                                Отменить ответ
                            </a>
                        </small>
                    </h3>
                    <div class="title-sep-container">
                        <div class="title-sep sep-double "></div>
                    </div>
                </div>
                <?php $form = ActiveForm::begin([
                    'id' => 'commentform',
                    'action' => ['comment', 'id' => $model->id],
                ]); ?>

                <div id="comment-input">
                    <?= $form->field($modelComment, 'name')->textInput([
                        'placeholder' => $modelComment->getAttributeLabel('name'),
                    ])->label(false) ?>

                    <?= $form->field($modelComment, 'email')->textInput([
                        'placeholder' => $modelComment->getAttributeLabel('email'),
                    ])->label(false) ?>
                </div>

                <?= $form->field($modelComment, 'comment', [
                    'options' => [
                        'id' => 'comment-textarea'
                    ],
                ])->textarea([
                    'placeholder' => $modelComment->getAttributeLabel('comment'),
                ])->label(false) ?>

                <?= Html::activeHiddenInput($modelComment, 'post_id') ?>

                <?= Html::activeHiddenInput($modelComment, 'parent_id') ?>

                <div class="form-submit">
                    <?= Html::submitInput('Отправить комментарий', [
                        'id' => 'comment-submit',
                        'class' => 'fusion-button fusion-button-default',
                    ]) ?>
                </div>

                <?php ActiveForm::end(); ?>
            </div>
        </div>
    <?php endif;?>
</div>