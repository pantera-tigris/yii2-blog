<?php

use himiklab\thumbnail\EasyThumbnailImage;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model singletonn\blog\models\BlogPosts */
/* @var $category singletonn\blog\models\BlogCategories */
?>
<div class="fusion-post-wrapper">
    <?php if ($model->image): ?>
        <div class="fusion-flexslider flexslider fusion-post-slideshow">
            <ul class="slides">

                <li style="width: 100%; float: left; margin-right: -100%; position: relative; opacity: 1; display: block; z-index: 2;"
                    class="flex-active-slide">
                    <div class="fusion-image-wrapper" aria-haspopup="true">
                        <a href="<?= $model->url ?>">
                            <?= Html::img($model->image(384, 208, EasyThumbnailImage::THUMBNAIL_OUTBOUND)) ?>
                        </a>
                    </div>
                </li>
            </ul>
        </div>
    <?php endif; ?>
    <div class="fusion-post-content-wrapper">
        <div class="fusion-post-content post-content">
            <h2 class="entry-title" data-fontsize="18" data-lineheight="27">
                <a href="<?= $model->url ?>">
                    <?= $model->title ?>
                </a>
            </h2>
            <p class="fusion-single-line-meta">
                <span><?= Yii::$app->formatter->asDate($model->published_at) ?></span>
                <?php foreach ($model->categories as $category): ?>
                    <span class="fusion-inline-sep">|</span>
                    <a href="<?= $category->url ?>" rel="category tag"><?= $category->title ?></a>
                <?php endforeach; ?>
                <span class="fusion-inline-sep">|</span>
                <span class="fusion-addinfo">
                    <span class="fusion-sviews">
                        <i class="fa fa-eye" aria-hidden="true"></i> <?= $model->views ?>
                    </span>
                </span>
            </p>
            <div class="fusion-content-sep"></div>
            <?php if ($model->description_short): ?>
                <div class="fusion-post-content-container">
                    <?= mb_strlen($model->description_short, 'UTF-8') > 200 ? mb_substr($model->description_short, 0, 200) . '...' : $model->description_short ?>
                </div>
            <?php endif; ?>
        </div>
        <div class="fusion-meta-info">
            <div class="fusion-alignleft">
                <a href="<?= $model->url ?>"
                   class="fusion-read-more">Читать дальше</a>
            </div>
            <?php if($this->context->module->isCommentsAllowed):?>
                <div class="fusion-alignright">
                    <a href="<?= $model->url ?>#comments">
                        <i class="fusion-icon-bubbles"></i>
                        <?php if ($model->getCommentCount()): ?>
                            <?= $model->getCommentCount() ?>
                        <?php else: ?>
                            &nbsp;Нет комментариев
                        <?php endif; ?>
                    </a>
                </div>
            <?php endif;?>
        </div>
    </div>
</div>