<?php

/* @var $this yii\web\View */
/* @var $model singletonn\blog\models\BlogCategories */
/* @var $searchModel singletonn\blog\models\BlogPostsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
?>
<?= \yii\widgets\ListView::widget([
    'id' => 'posts-container',
    'dataProvider' => $dataProvider,
    'itemView' => '@blog/views/posts/_view',
    'itemOptions' => [
        'class' => 'fusion-post-grid post'
    ],
    'layout' => '<div class="blog-post-items">{items}</div>{pager}',
    'pager' => [
        'class' => \kop\y2sp\ScrollPager::className(),
        'enabledExtensions' => [
            \kop\y2sp\ScrollPager::EXTENSION_TRIGGER,
        ],
        'eventOnRendered' => new \yii\web\JsExpression('function(){
            blogMasonry.masonry("reloadItems");
            blogMasonry.masonry();
        }'),
        'item' => '.post',
        'delay' => 0,
    ]
]) ?>