<?php

namespace singletonn\blog;

use Yii;
use yii\base\InvalidConfigException;

class Module extends \yii\base\Module
{

    public $mainImagePath;
    public $mainImageUrl;
    public $editorImagesPath;
    public $urlPrefix = 'blog';
    public $urlSuffix = '';
    public $quality = 50;
    public $urlRules = [
        'tag/<alias:[\w-\s]+>' => 'tag/view',
        [
            'class' => 'singletonn\blog\components\UrlManager'
        ],
    ];
    public $titleCallback;
    public $isCommentsAllowed = true;
    /* @var string Событие перед рендерингом просмотра записи */
    const EVENT_PAGE_VIEW = 'eventPageView';
    /* @var string Событие перед рендерингом списка по категории */
    const EVENT_CATEGORY_VIEW = 'eventCategoryView';
    /* @var string Событие перед рендерингом списка по тегу */
    const EVENT_TAG_VIEW = 'eventTagView';

    public function init()
    {
        parent::init();
        Yii::setAlias('@blog', __DIR__);
        if (!$this->mainImagePath) {
            throw new InvalidConfigException('Опция mainImagePath обязательная');
        }
        if (!$this->editorImagesPath) {
            throw new InvalidConfigException('Опция editorImagesPath обязательная');
        }
        Yii::$app->controllerMap['elfinder-blog'] = [
            'class' => 'mihaildev\elfinder\Controller',
            'access' => ['@'],
            'disabledCommands' => ['netmount'],
            'roots' => [
                [
                    'baseUrl' => '@web',
                    'basePath' => '@webroot',
                    'path' => $this->editorImagesPath,
                    'name' => 'Картинки'
                ],
            ],
        ];
    }

}