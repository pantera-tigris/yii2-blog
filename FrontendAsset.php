<?php

namespace singletonn\blog;

use yii\web\AssetBundle;

/**
 * Main backend application asset bundle.
 */
class FrontendAsset extends AssetBundle
{
    public $sourcePath = '@blog/assets';
    public $css = [
        'css/frontend.css'
    ];
    public $js = [
        '//yastatic.net/es5-shims/0.0.2/es5-shims.min.js',
        '//yastatic.net/share2/share.js',
        'js/masonry.pkgd.min.js',
        'js/script.js',
    ];
    public $depends = [
        'yii\web\YiiAsset',
    ];
}
